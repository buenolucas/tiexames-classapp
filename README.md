#tiexames-classapp

# tiexames-classapp

ClassApp para TIExames

## Templates

Verificar os arquivos 

### `public\index.html`

Html que inicia a aplicação. 

### `public\content.html`

Modelo de página para exibição de video.

## Primeiros Passos

1. fazer donwload do repositório
2. abrir o terminal
3. entrar na pasta 
4. executar o comando `yarn`

## Scripts Disponíveis

Dentro da pasta do projeto você pode executar:

### `yarn start`
Executa o modo de desenvolvimento.<br/>
Abra [http://localhost:3000](http://localhost:3000) para visualizar no navegador.

### `yarn build`

Faz um build da aplicação na pasta `build`.<br />
Cria os pacotes de arquivos corretamente e otimiza para o ambiente de produção;


