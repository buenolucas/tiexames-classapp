import { combineReducers } from 'redux';
import course from '../course/reducer';

export enum EReduxActionTypes {
    GET_CLASS = 'GET_CLASS',
    SET_TOPIC = 'SET_TOPIC'
}

export interface IReduxBaseAction {
    type: EReduxActionTypes;
}

const rootReducer = combineReducers({
    course
});

export type AppState = ReturnType<typeof rootReducer>;

export default rootReducer;
