import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { AnyAction, bindActionCreators, Dispatch } from 'redux';
import { AppState } from '../helpers/rootReducer';
import { getClass } from '../course/actions';
import Sidebar from './Sidebar';
import AppBar from './AppBar';
import ContentArea from './ContentArea';

//import styled, { ThemeProvider } from 'styled-components'
//import theme from '../theme';

import styled, { ThemeProvider, theme, createGlobalStyle } from "../theme";
import { IClass } from '../course/reducer';



const GlobalStyle = createGlobalStyle`
    
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    
    body {
        font-family: ${props => props.theme.fontFamily};
        color: ${props => props.theme.colors.body};
        font-size: ${props => props.theme.fontSize};
        height:100%;
    }
    #root {
        display:flex;
        flex-direction:column;
        height:100%;
    }
`
const AppViewport = styled.div`
    display:flex;
    flex-direction:row;
    flex-grow:1;
`

class RootContainer extends PureComponent<ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>, {}> {
    public componentDidMount() {
        const { getClass, isLoaded } = this.props;
        if (!isLoaded) {
            getClass();
        }
      }

    render() {
        const { isLoaded, course } = this.props;
        if(!isLoaded) {
            return <div>Loading</div>
        } else {
            return (
                <React.Fragment>
                    <AppBar title={course.title}/>
                    <AppViewport>
                        <Sidebar/>
                        <ContentArea/>
                    </AppViewport>
                </React.Fragment>
            );
        }
    }
}


const mapStateToProps = (state: any) => ({
    course: state.course.course,
    isLoaded: state.course.classLoaded,
});



const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
    bindActionCreators(
    {
        getClass
    },
    dispatch
  );

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(RootContainer);

