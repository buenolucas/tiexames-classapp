import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled, { css } from '../theme';

const AccordeonSkin = styled.div`
    height:100%;
    backgound-color:#FF0000/
`
const HeaderSkin = styled.div`
    background-color:#F6F6F6;
    padding-top:${(props:any) => props.theme.unit};
    padding-bottom:${(props:any) => props.theme.unit};
    padding-left: ${(props:any) => props.theme.unit};
    padding-right: ${(props:any) => props.theme.unit};  
    font-weight: ${props => props.theme.fontWeight.bold}; 
    display:flex;
    flex-direction: row;
`
const Title = styled.div`
    flex-grow:1;
`
const Icon = styled.div`
    margin-left:12px;
`

type ItemSkinType = {
    selected?: boolean
  }
const ItemSkin = styled.div<ItemSkinType>`
    padding-top:7px;
    padding-bottom:7px;
    padding-left: ${(props:any) => props.theme.unit};
    padding-right: ${(props:any) => props.theme.unit};
    display:flex;
    cursor:pointer;
    ${(props:any) =>
        props.selected &&
        css`
            border-left:4px solid ${(props:any) => props.theme.colors.primary}
        `}
`

const ItemCheckSkin = styled.div`
    font-weight: ${props => props.theme.fontWeight.light};
    font-size: 15px;
    margin-right:15px;
`
const ItemContentSkin = styled.div`
    font-weight: ${props => props.theme.fontWeight.light};
    font-size: 15px;
`

const ItemTitleSkin = styled.div`
    font-weight: ${props => props.theme.fontWeight.normal};
`
const ItemDurationSkin = styled.div`
    font-weight: ${props => props.theme.fontWeight.light};
    font-size: 15px;
`




interface IHeader {
    onToggleClick: any;
    title: string;
    visible: boolean;
}

class Header extends Component<IHeader> {
    render() {
        const { title, onToggleClick, visible } = this.props;
        return (
            <HeaderSkin>
                <Title>{title}</Title>
                <Icon>
                    {visible ? 
                        <svg width="17" height="9" viewBox="0 0 17 9" fill="none" xmlns="http://www.w3.org/2000/svg" onClick={onToggleClick}>
                             <path d="M1 8.5L8.5 1L16 8.5" stroke="black"/>
                        </svg>
                        :
                        <svg width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg" onClick={onToggleClick}>
                            <path d="M1 1L8.5 8.5L16 1" stroke="black"/>
                        </svg>
                    }
                </Icon>
            </HeaderSkin>
        );
    }
}

interface IItemRenderer {
    title: string;
    duration: string;
    selected?: boolean;
    checked?: boolean;
    onClick: any;
}

export class AccordeonItem extends Component<IItemRenderer, {}> {
    onClick(ee:any) {
        console.log(ee)
    }
    render() {
        const {title, duration, selected} = this.props;
        return (
            <ItemSkin selected={selected} onClick={this.props.onClick}>
                <ItemCheckSkin>
                    <input type="checkbox"/>
                </ItemCheckSkin>
                <ItemContentSkin>
                    <ItemTitleSkin>{title}</ItemTitleSkin>
                    <ItemDurationSkin>{duration}</ItemDurationSkin>
               </ItemContentSkin>

            </ItemSkin>
        );
    }
}

interface ISectionState {
    visible:boolean
}
interface ISection {
    title:string;
}

type SectionProps = {
    title:string;
};
type SectionState = {
    visible:boolean

};

export class AccordeonSection extends Component<SectionProps, SectionState> {
    state = { visible: true };
    toggleHandler() {
        console.log('xablay')
        this.setState({visible: !this.state.visible });
    }
    render() {
        const {visible} = this.state;
        console.log('cablay')
        const {title, children} = this.props;
        return (
            <div>
                <Header 
                    title={title}
                    visible={visible}
                    onToggleClick={()=>this.toggleHandler()}
                />
                <div style={{display: visible ? 'block' : 'none'}}>
                    {children}
               </div>
            </div>
        )
    }
} 

class Accordeon extends Component {
    
    render() {
        const {children} = this.props;
        return (
            <AccordeonSkin>
                {children}
            </AccordeonSkin>
        );
    }
    
}

export default Accordeon;
