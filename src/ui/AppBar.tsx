import React, { Component } from 'react';
import styled from 'styled-components'

const AppBarSkin = styled.div`
    background-color: ${props => props.theme.colors.primary};
    font-family:  ${props => props.theme.fontFamily};
    font-weight: ${props => props.theme.fontWeight.bold};
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    color: #ffffff;
    display:flex;
    height:82px;
    align-items:center;
`

const Inner = styled.div`
    display:flex;
    margin-left:20px;
    margin-right:20px;
    align-items:center;
    width:100%;
    font-size:12px;
`
type Props = {
    title?: string;
}

const AppBar: React.FC<Props> = ({ title}) =>
    <AppBarSkin>
        <Inner>
            <h1>{title}</h1>
        </Inner>
    </AppBarSkin>

/*
class AppBar extends Component<IClass> {
    render() {
        {title} = this.props.title
        return (
            <AppBarSkin>
                <Inner>
                    <h1>{title}</h1>
                </Inner>
            </AppBarSkin>
        );
    }
}
*/

export default AppBar;
