import React from 'react';
import styled from '../../theme';

const ButtonSkin = styled.button`
    margin-top: .25rem;
    margin-bottom: .25rem;
    color: #fff;
    background-color: transparent;
    border-color: transparent;
    display: inline-block;
    font-weight: 300;
    text-align: center;
    vertical-align: middle;
    cursor: pointer;
    user-select: none;
    padding: .375rem 0;
    font-size: 1.2rem;
    line-height: 1.5;
    border-radius: .25rem;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
     
    &:hover {
        text-decoration:underline;
    }
    >svg{
        vertical-align:middle;
    }

`
const ButtonLabel = styled.label`
    vertical-align:middle;

`
export const BaseButton = ()=> {
    return (
        <ButtonSkin>

        </ButtonSkin>
    )
}

export const NextButton = ()=> {
    return (
        <ButtonSkin>
            <ButtonLabel>
                Próximo
            </ButtonLabel>
            <svg width="8" height="21" viewBox="0 0 8 21" fill="none" xmlns="http://www.w3.org/2000/svg" style={{marginLeft:"12px"}}>
                <path d="M0 20L7 10.5L1.66103e-06 1" stroke="white" />
            </svg>
        </ButtonSkin>
    )

}

export const PreviousButton = ()=> {
    return (
        <ButtonSkin>
            <svg width="9" height="21" viewBox="0 0 9 21" fill="none" xmlns="http://www.w3.org/2000/svg" style={{marginRight:"12px"}}>
                <path d="M8 1L1 10.5L8 20" stroke="white"/>
            </svg>
            <ButtonLabel>
                Anterior
            </ButtonLabel>
        </ButtonSkin>
    )

}