import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { AnyAction, bindActionCreators, Dispatch } from 'redux';
import { AppState } from '../helpers/rootReducer';
import styled from '../theme';
import Accordeon, {AccordeonSection, AccordeonItem} from './Accordeon';
import { setTopic } from '../course/actions';
const SidebarSkin = styled.div`
    width:345px;
`

const SidebarHeader = styled.div`
    padding-top:${(props:any) => props.theme.unit};
    padding-bottom:${(props:any) => props.theme.unit};
    padding-left: ${(props:any) => props.theme.unit};
    padding-right: ${(props:any) => props.theme.unit};
    width:345px;
    font-weight: ${props => props.theme.fontWeight.bold};
    font-size:21px;
    border-bottom:4px solid #C4C4C4;
    display:flex;
    flex-direction:row;
`
const Title = styled.div`
flex-grow:1;

`
const Icon = styled.div`

`
class Sidebar extends PureComponent<ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>, {}> {
    onItemClickHandler(e:any) {
        this.props.setTopic(e)
    }
    render() {
        const {course, selecetdTopicId} = this.props;
        let cc=0;
        return (
        <SidebarSkin>
            <SidebarHeader>
                <Title>Conteúdo do Curso</Title>
                <Icon>
                    <svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1 18.25L17.7927 1" stroke="black"/>
                        <path d="M18 18.25L1.20732 1" stroke="black"/>
                    </svg>
                </Icon>
            </SidebarHeader>
            <Accordeon>
                { course.sections.map((section:any, index: number) => (
                    <AccordeonSection title={section.title} key={'section'+index}>
                        { section.topics.map((topic:any, topicIndex: number) => {
                            cc++;
                            return (
                                <AccordeonItem 
                                    key={'section'+cc} 
                                    title={topic.title} 
                                    duration={topic.duration} 
                                    selected={selecetdTopicId == topic.id && true} 
                                    checked={topic.checked}
                                    onClick={(e:any)=>this.onItemClickHandler(topic)}
                                />
                            )}
                        )}
                    </AccordeonSection>
                ))}
            </Accordeon>
        </SidebarSkin>
        );
    }
}


const mapStateToProps = (state: any) => ({
    course: state.course.course,
    selecetdTopicId: state.course.selectedTopicId,
});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
    bindActionCreators( 
        {
            setTopic
        },
        dispatch
  );

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(Sidebar);

