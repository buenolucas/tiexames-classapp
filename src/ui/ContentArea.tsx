import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { AnyAction, bindActionCreators, Dispatch } from 'redux';
import styled from '../theme';
import ContentFrame from './ContentFrame';
import { NextButton, PreviousButton } from './componets/buttons';

const ContentWrapper = styled.div`
    flex-grow:1;
    display:flex;
    flex-direction:column;
`

const Content = styled.div`
    flex-grow:1;
    display:flex;
`

const ContentHeader = styled.div`
    background-color:#2C2C2C;
    color:#FFFFFF;
`
const ContentHeaderWraper = styled.div`
   display:flex;
   flex-direction:column;
   padding-left:42px;
   padding-right:42px;
   padding-top:42px;
   padding-bottom:42px;
`
const HeaderTitle = styled.h2`
   font-weight: ${props => props.theme.fontWeight.light};
   font-size: 42px;
`
const HeaderActions = styled.div`
   font-weight: ${props => props.theme.fontWeight.light};
   font-size: 42px;
   display:flex;
   justify-content: space-between;
`
const NavigatorButton = styled.button`

`

class ContentArea extends PureComponent<ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>, {}> {
    render() {
        const {selectedTopic} = this.props;
        return (
            <ContentWrapper>
                <Content>
                    <ContentFrame className='iframe' url={selectedTopic.url} frameBorder={0} />
                </Content>
                <ContentHeader>
                    <ContentHeaderWraper>
                        <HeaderTitle>{selectedTopic.title}</HeaderTitle>
                        <HeaderActions>
                            <PreviousButton/>
                            <NextButton/>
                        </HeaderActions>
                    </ContentHeaderWraper>
                </ContentHeader>
            </ContentWrapper>
        );
    }
}



const mapStateToProps = (state: any) => ({
    selectedTopic: state.course.selectedTopic,
});

const mapDispatchToProps = (dispatch: Dispatch<AnyAction>) =>
    bindActionCreators( 
        {  },
        dispatch
  );

export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(ContentArea);


