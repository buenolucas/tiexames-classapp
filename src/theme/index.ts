import * as styledComponents from "styled-components";

const {
    default: styled,
    css,
    createGlobalStyle,
    keyframes,
    ThemeProvider
} = styledComponents



export const theme = {
    borderRadius: '0px',
    fontFamily: "Roboto",
    fontSize: '16px',
    unit:'22px',
    fontWeight: {
        bold: '700',
        normal: '400',
        light: '300',
    },
    colors: {
        primary: "#19396f",
        body: '#646464',
    }

}

export default styled;
export { css, keyframes, createGlobalStyle, ThemeProvider };