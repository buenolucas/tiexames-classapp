import { EReduxActionTypes } from './../helpers/rootReducer';
import { IReduxGetClassAction, IReduxSetTopicAction } from './actions';

export interface ITopic {
    id: string;
    title: string;
    duration: string;
    url: string;
    checked?: boolean;
    selected?: boolean;
}
export interface ISection {
    id: string;
    title: string;
    topics: ITopic[]
    collapsed?: boolean;
}
export interface IClass {
    title: string;
    sections: ISection[]
}
export interface IReduxClassState {
    course: any;
    classLoaded: boolean;
    selectedTopic: ITopic;
    selectedTopicId: string;

}
const initialState: IReduxClassState = {
    course: { title: 'Null', sections: [{ title: 'null', topics: [{ title: 'null', duration: '0min', url: 'http://' }] }] },
    classLoaded: false,
    selectedTopic: { id: '  aaa', title: 'n', duration: '0', url: 'http://' },
    selectedTopicId: 'aaa'
}

type TClassReducerActions = IReduxGetClassAction | IReduxSetTopicAction;

export default function (state: IReduxClassState = initialState, action: TClassReducerActions) {

    switch (action.type) {
        case EReduxActionTypes.GET_CLASS:
            const st: ITopic = action.data.sections[0].topics[0];
            return { ...state, course: action.data, classLoaded: true, selectedTopic: st, selectedTopicId: st.id };
        case EReduxActionTypes.SET_TOPIC:
            return { ...state, selectedTopic: action.data, selectedTopicId: action.data.id };
        default:
            return state;
    }

}

