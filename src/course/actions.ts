import { ThunkAction, ThunkDispatch } from 'redux-thunk';

import { IReduxBaseAction, EReduxActionTypes } from '../helpers/rootReducer';
import { IReduxClassState, IClass, ISection, ITopic } from './reducer';
import uuid from '../helpers/uuid';

export interface IReduxGetClassAction extends IReduxBaseAction {
    type: EReduxActionTypes.GET_CLASS;
    data: IClass;
}
export interface IReduxSetTopicAction extends IReduxBaseAction {
    type: EReduxActionTypes.SET_TOPIC;
    data: ITopic;
}


export function setTopic(topic: ITopic): IReduxSetTopicAction {
    return {
        type: EReduxActionTypes.SET_TOPIC,
        data: topic
    };
}

export function getClass(): IReduxGetClassAction {
    var dt = window['classData'];
    dt.sections = dt.sections.map((section: ISection) => {
        section.topics = section.topics.map((topic: ITopic) => {
            return { ...topic, id: uuid() }
        })
        return { ...section, id: uuid() }
    })
    return {
        type: EReduxActionTypes.GET_CLASS,
        data: dt
    };
}
/*
export function getClass(): ThunkAction<
    Promise<IReduxGetClassAction>,
    IReduxClassState,
    undefined,
    IReduxGetClassAction
> {
    return async (dispatch: ThunkDispatch<IReduxClassState, undefined, IReduxGetClassAction>) => {


        return dispatch({
            type: EReduxActionTypes.GET_CLASS,
            data: window['classData']
        });
    };
}*/