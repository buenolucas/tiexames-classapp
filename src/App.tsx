import React from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import reduxLogger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
import  {ThemeProvider, theme, createGlobalStyle } from "./theme";
import RootContainer from './ui/RootContainer';
import rootReducer from './helpers/rootReducer';

const GlobalStyle = createGlobalStyle`
    
  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }
    * { margin: 0; padding: 0; }
    html, body {
        width:100%;
        height:100%;
    }
    body {
        font-family: ${(props:any) => props.theme.fontFamily};
        color: ${(props:any) => props.theme.colors.body};
        font-size: ${(props:any) => props.theme.fontSize};
        font-weight: ${(props:any) => props.theme.fontWeight.normal};
    }
    #root {
        display:flex;
        flex-direction:column;
        height:100%;
    }
    .iframe {
      border:0;
      width:100%;
      height:100%;
    }
`

const middleware = [reduxLogger, thunk];

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(...middleware)));

function App() {
  return (
    <Provider store={store}>
      <React.Fragment>
        <ThemeProvider theme={theme}>
          <GlobalStyle/>
          <RootContainer/>
        </ThemeProvider>
      </React.Fragment>
    </Provider>
  )
}

export default App;
